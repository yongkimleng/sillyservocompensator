#include <Servo.h>

#include "PID_v1.h"

// Define which pins are connected to the servo
#define INPUT_PIN   A0
#define OUTPUT_PIN  9

// Set the ID of this servo controller
const byte NODE_ID = 0;

// hardcoded movement limits, regardless of setpoint
const int LOWER_LIMIT = -500;
const int UPPER_LIMIT = 500;

const int OUTPUT_DEADBAND = 2; // range in which we consider turning off motor
const int INPUT_DEADBAND = 1;
const int INPUT_VEL_DEADBAND = 2;

bool servo_on = false;    // flag if the servo is intentionally shut down or not
bool servo_sleep = true;  // flag if the servo is temporarily stopped
bool send_done = true;    // flag if the DONE msg was sent

// due to the nature of shitty chinese servo, we have 2 different output modes
bool input_following = false;

// Define Variables that would go into the PID class
double Setpoint, Input, Output;
PID myPID(&Input, &Output, &Setpoint, 0, 0, 0, DIRECT);

// threshold to differentiate between major and minor
const int diffMinor = 100;

// Different PID constants for different movement segments
void setCWTuningsMinor()  { myPID.SetTunings(6, 6, 0.02); input_following = false; }
void setCWTuningsMajor()  { myPID.SetTunings(4, 1, 0.1); input_following = true; }

void setACWTuningsMinor() { myPID.SetTunings(4, 5, 0.02); input_following = false; }
void setACWTuningsMajor() { myPID.SetTunings(7, 1, 0.1); input_following = true; }

uint32_t serialTime;

int outp_val;
int curr_setpoint;
int curr_input;

Servo outp;

void setup() {
  Serial.begin(115200);
  Serial1.begin(9600);

  myPID.SetOutputLimits(LOWER_LIMIT, UPPER_LIMIT);

  // pid on
  myPID.SetMode(AUTOMATIC);
}
  
void loop() {

  calc_servo();
  parse_serial();
}

int mappedRead() {
  // Tune this depending on the range of analog input
  return map(analogRead(INPUT_PIN), 100, 650, 0, 500);
}

void set_point(int pos) {
  curr_setpoint = pos;
  outp.attach(OUTPUT_PIN);
  servo_sleep = false;
  servo_on = true;
  send_done = false;
}

void reached_point() {
  if(!send_done) {
    serial_send_ok();
    send_done = true;
  }
}

void shutdown_servo() {
  servo_on = false;
  outp.detach();
  serial_send_ok();
}

void calc_servo() {
  static bool computed;
  static int prev_outp_val;
  static int outp_diff;
  static int last_input;

  // input of 10-500~
  curr_input = mappedRead();
  
  if(servo_on && !servo_sleep) {
    
    Input = curr_input;
    Setpoint = curr_setpoint;

    if(curr_setpoint < curr_input) {
      if(curr_setpoint < curr_input - diffMinor) {
        setACWTuningsMajor();
      } else {
        setACWTuningsMinor();
      }
    }
    else
    {
      if(curr_setpoint > curr_input + diffMinor) {
        setCWTuningsMajor();
      } else {
        setCWTuningsMinor();
      }
    }
    
    computed = myPID.Compute();

    if(computed) {
      // Output is +-
      // + Input;
      outp_val = Output;

      if(within_range(curr_input, last_input, INPUT_VEL_DEADBAND)) {    // velocity ~= 0
        if(within_range(curr_input, curr_setpoint, INPUT_DEADBAND)) {   // position diff ~= 0
          if(within_range(outp_val, prev_outp_val, OUTPUT_DEADBAND)) {
            servo_sleep = true;   // output velocity ~= 0
          }
          reached_point();
        }
      }
        
      prev_outp_val = outp_val;
      last_input = curr_input;

      // outp_val is -500-500
      if(input_following) {
        // input-following mode: add input position and map to range 1000-2000
        outp_val = outp_val + (last_input*2) + 1000;
      } else {
        // map outp_val to range 1000-2000
        outp_val += 1500;
      }
      if(outp_val > 2000) outp_val = 2000;
      else if(outp_val < 1000) outp_val = 1000;
      
      outp.writeMicroseconds(outp_val);
    }
  
  } else {
    // "detach" so that the servo stops applying power to the motor. whining is annoying.
    outp.detach();
    if(!within_range(curr_input, curr_setpoint, INPUT_DEADBAND)) {
      // if, for some reason the angle is off, wake up and get back into action
      servo_sleep = false;
      outp.attach(OUTPUT_PIN);
    }
  }
}


// Frame format:
// 1. 0xAA
// 2. NODE_ID (default: 0)
// 3. value (upper byte)
// 4. value (lower byte)
void parse_serial() {
  static byte frame[4];
  static byte frame_i = 0;
  static int16_t value;
  if(Serial.available() || Serial1.available()) {
    if(Serial.available()) frame[frame_i++] = Serial.read();
    else frame[frame_i++] = Serial1.read();

    switch(frame_i) {
      case 1:
        if(frame[0] != 0xAA) frame_i = 0; // discard
        break;
      
      case 4:
        // end of frame
        frame_i = 0;
        if(frame[1] == NODE_ID) {
          value = (frame[3] << 8) & 0xFF00; // upper
          value |= frame[4];                // lower
          
          if(value >= 0 && value <= 500) {
            set_point(value);
          } else if(value == -1) {
            // turn off servo
            shutdown_servo();
          }
        }
        break;
    }
  }
}

void serial_send_err() {
  if(Serial) Serial.print("E");
  Serial1.print("E");
}

void serial_send_ok() {
  if(Serial) Serial.print("!");
  Serial1.print("!");
}

bool within_range(int v1, int v2, int r) {
  int diff = abs(v1-v2);  
  return (diff <= r);
}

